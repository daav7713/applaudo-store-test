You can install the site in several ways

  1. Manually installation
    * Clone the site from its repo https://gitlab.com/daav7713/applaudo-store-test
    * Copy the wp-config-sample.php file to wp-config.php
    * Configure your database in the wp-config file
    * Create a MYSQL databse
    * Get the dump.sql file and open in atom or vscode (preferably I recommend vscode)
    * press crtl + f to search for the original domain of the site (da-av.xyz) and replace all to the new domain of your installation
    * Import the dump file to you created mysql database
    * Test the site.

  2. Via All-in-one WP migration plugin:
    * First go to http://da-av.xyz/admin.
    * In the admin menu go to the All-in-one WP migration section.
    * Click in the export tab
    * Click in Export to -> File
    * Download the file.
    * Go to the site where you want to migrate wordpress (you must have a fresh wordpress installation with WP migration plugin included)
    * In the admin menu go to the All-in-one WP migration section.
    * Drag or click in Import from (File) to upload the exported file from wordpress
    * Wait for the results
