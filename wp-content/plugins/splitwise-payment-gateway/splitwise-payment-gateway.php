<?php
/*
 * Plugin Name: WooCommerce Splitwise Payment Gateway
 * Plugin URI: http://localhost:8000
 * Description: Take payments on your store via splitwise api.
 * Author: Diego Arana
 * Author URI: http://da-av.xyz
 * Version: 1.0.1
 *
 /*
  * This action hook registers our PHP class as a WooCommerce payment gateway
  */
date_default_timezone_set('America/El_Salvador');
require 'vendor/autoload.php';

 add_filter( 'woocommerce_payment_gateways', 'splitwise_add_gateway_class' );
 function splitwise_add_gateway_class( $gateways ) {
 	$gateways[] = 'WC_Splitwise_Gateway'; // your class name is here
 	return $gateways;
 }

 /*
  * The class itself, please note that it is inside plugins_loaded action hook
  */
 add_action( 'plugins_loaded', 'splitwise_init_gateway_class' );

 function splitwise_init_gateway_class() {

   class WC_Splitwise_Gateway extends WC_Payment_Gateway {
      /**
      * Class constructor
      */
      public function __construct() {

      	$this->id = 'splitwise'; // payment gateway plugin ID
      	$this->icon = ''; // URL of the icon that will be displayed on checkout page near your gateway name
      	$this->has_fields = false; // in case you need a custom credit card form
      	$this->method_title = 'Woocommerce Splitwise Gateway';
      	$this->method_description = 'Payment gateway via splitwise api'; // will be displayed on the options page

      	// gateways can support subscriptions, refunds, saved payment methods,
      	// but in this tutorial we begin with simple payments
      	$this->supports = array(
      		'products'
      	);

      	// Method with all the options fields
      	$this->init_form_fields();

      	// Load the settings.
      	$this->init_settings();
      	$this->title = $this->get_option( 'title' );
      	$this->description = $this->get_option( 'description' );
      	$this->enabled = $this->get_option( 'enabled' );
      	$this->testmode = 'yes' === $this->get_option( 'testmode' );
      	$this->consumer_key = $this->get_option( 'splitwise_consumer_key' );
      	$this->consumer_secret = $this->get_option( 'splitwise_consumer_secret' );
      	$this->consumer_callback = $this->get_option( 'splitwise_consumer_callback' );

      	// This action hook saves the settings
      	add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

      	// We need custom JavaScript to obtain a token
      	add_action( 'wp_enqueue_scripts', array( $this, 'payment_scripts' ) );

      	// You can also register a webhook here
        add_action( 'woocommerce_api_wc_gateway_splitwise', array( $this, 'check_splitwise_response' ) );
      }

      /**
       * Plugin options, we deal with it in Step 3 too
       */
      public function init_form_fields(){
        $this->form_fields = array(
          'enabled' => array(
            'title'       => 'Enable/Disable',
            'label'       => 'Enable Splitwise Gateway',
            'type'        => 'checkbox',
            'description' => '',
            'default'     => 'no'
          ),
          'title' => array(
            'title'       => 'Title',
            'type'        => 'text',
            'description' => 'This controls the title which the user sees during checkout.',
            'default'     => 'Connect to yout Splitwise Account',
            'desc_tip'    => true,
          ),
          'description' => array(
            'title'       => 'Description',
            'type'        => 'textarea',
            'description' => 'This controls the description which the user sees during checkout.',
            'default'     => 'Login with your account to process the payment',
          ),
          'testmode' => array(
            'title'       => 'Test mode',
            'label'       => 'Enable Test Mode',
            'type'        => 'checkbox',
            'description' => 'Place the payment gateway in test mode using test API keys.',
            'default'     => 'yes',
            'desc_tip'    => true,
          ),
          'splitwise_consumer_key' => array(
            'title'       => 'Splitwise Consumer Key',
            'type'        => 'text'
          ),
          'splitwise_consumer_secret' => array(
            'title'       => 'Splitwise Consumer Secret',
            'type'        => 'password',
          ),
          'splitwise_consumer_callback' => array(
            'title'       => 'Splitwise Consumer Callback',
            'type'        => 'text',
          )
        );
    	}

      /**
       * You will need it if you want your custom credit card form, Step 4 is about it
       */
      public function payment_fields() {
        // ok, let's display some description before the payment form
        if ( $this->description ) {
          // display the description with <p> tags etc.
          echo wpautop( wp_kses_post( $this->description ) );
        }

      }

     /*
     * Custom CSS and JS, in most cases required only when you decided to go with a custom credit card form
     */
    	public function payment_scripts() {

    	}

      /*
       * Fields validation, more in Step 5
      */
      public function validate_fields() {
      }

      /*
      * Function
      */
      /*
        * We're processing the payments here.
      */
      public function process_payment( $order_id ) {
        global $woocommerce;
        $order = new WC_Order( $order_id );
        // Mark as on-hold (we're awaiting the cheque)
        $order->update_status('on-hold', __( 'Awaiting splitwise payment', 'woocommerce' ));

        $signer = new Risan\OAuth1\Signature\HmacSha1Signer();
        $oauth1 = Risan\OAuth1\OAuth1Factory::create([
            'client_credentials_identifier' => $this->consumer_key,
            'client_credentials_secret' => $this->consumer_secret,
            'temporary_credentials_uri'     => 'https://secure.splitwise.com/oauth/request_token',
            'authorization_uri'             => 'https://secure.splitwise.com/oauth/authorize',
            'token_credentials_uri'         => 'https://secure.splitwise.com/oauth/access_token',
            'callback_uri' => $this->consumer_callback,
        ],$signer);
        //

        $temporaryCredentials = $oauth1->requestTemporaryCredentials();
        // Store the temporary credentials and order_id in session.
        $_SESSION['temp_credentials'] = serialize($temporaryCredentials);
        $_SESSION['order_id'] = $order_id;
        // Generate and redirect user to authorization URI.
        $authorizationUri = $oauth1->buildAuthorizationUri($temporaryCredentials);
        $result = array(
            'result' => 'success',
            'redirect' => $authorizationUri
        );

        return $result;
    	}

      public function check_splitwise_response(){
        if (isset($_GET['oauth_token']) && isset($_GET['oauth_verifier'])) {
          $order = wc_get_order( $_SESSION['order_id'] );
          $signer = new Risan\OAuth1\Signature\HmacSha1Signer();
          $oauth1 = Risan\OAuth1\OAuth1Factory::create([
              'client_credentials_identifier' => $this->consumer_key,
              'client_credentials_secret' => $this->consumer_secret,
              'temporary_credentials_uri'     => 'https://secure.splitwise.com/oauth/request_token',
              'authorization_uri'             => 'https://secure.splitwise.com/oauth/authorize',
              'token_credentials_uri'         => 'https://secure.splitwise.com/oauth/access_token',
              'callback_uri' => $this->consumer_callback,
          ],$signer);
          try{
            $tokenCredentials = $oauth1->requestTokenCredentials(
              unserialize($_SESSION['temp_credentials']),
              $_GET['oauth_token'],
              $_GET['oauth_verifier']
            );
            unset($_SESSION['temp_credentials']);
            unset($_SESSION['order_id']);
            if ($tokenCredentials) {
              $oauth1->setTokenCredentials($tokenCredentials);
              $urlTesting ='https://secure.splitwise.com/api/v3.0/test';
              $testInfo = $oauth1->request('GET', $urlTesting);
              $testArray = json_decode($testInfo->getBody()->getContents(), true);
              //create expense
              $urlToCreateAExpense = 'https://secure.splitwise.com/api/v3.0/create_expense';
              $headers_to_create_a_expense = array(
                  'oauth_consumer_key'      => '3CeLgTGDwrq4VYmzfpP8P3PwbL7npd4U30F23pue',
                  'oauth_token'             => $testArray['token']['secret'],
                  'Authorization'           => 'Bearer '.$testArray['token']['secret'],
                  'Content-Type'            => 'application/x-www-form-urlencoded'
              );

              $parametersToCreateAExpense = array(
                  'cost'                  => (float) $order->get_total(),
                  'description'           => "Compra en mi tienda",
                  'payment'               => (float) $order->get_total(),
                  'users__1__user_id'     => $testArray['token']['user_id'],
                  'users__1__paid_share'  => (float) $order->get_total(),
                  'users__1__owed_share'  => (float) $order->get_total()
              );

              $responseRequest = $oauth1->request('POST', $urlToCreateAExpense, [
                  'form_params' => $parametersToCreateAExpense,
              ]);

              $res = json_decode($responseRequest->getBody()->getContents(), true);
              if(isset($res['expenses'][0]['id'])){
                wp_redirect(html_entity_decode ( $order->get_checkout_order_received_url() ));
                return;
              }
            }else{
              wp_redirect('/');
            }
          }catch (Exception $e) {
              echo 'Excepción capturada: ',  $e->getMessage(), "\n";
          }
        }else{
          wp_redirect('/');
        }
      }

    }
 }
