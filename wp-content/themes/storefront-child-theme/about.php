<?php
/**
* Template Name: Aboutme
*
* @package WordPress
* @subpackage Storefron Child theme
* @since Storefront child theme
*/
get_header(); ?>

	<div id="primary" class="container-fluid">
		<main id="main" class="site-main" role="main">
      <div class="container-full">
        <div class="row text-justify">
          <?php
          while ( have_posts() ) :
            the_post();

            get_template_part( 'content', 'page' );


          endwhile; // End of the loop.
          ?>
        </div>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
