<?php
/*
* Storefront Child theme functions
*
*/

function register_my_session() {
    if (!session_id()) {
        session_start();
    }
}
add_action('init', 'register_my_session');
